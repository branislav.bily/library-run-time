# RunTime Library

## Installation

Clone git repository. Run `make` to compile needed files.
If you want to test if the library is running, you can run `./runtime_libraryTest`.

### For macOS

Copy `libraryRunTime.h` into `/usr/local/include` folder. Make sure you have admin privileges.<br>
Copy `libRunTime.dylib` into `/usr/local/lib` folder. Make sure you have admin privileges.

### For Linux

Follow this [tutorial](https://www.cprogramming.com/tutorial/shared-libraries-linux-gcc.html).

### For Windows

I have no idea good luck with that.

## Usage

On top of your C file, enter `#include "libraryRunTime.h"`.<br>

When you want to start the timer, enter `startTimer();`.
To end the timer, enter `endTimer();`.<br> `endTimer()` returns `double`, duration in seconds.

### Example

```c
#include <stdio.h>
#include "libraryRunTime.h"

int main() {
  startTimer();

  // Perform your code here

  double time = endTimer();
  printf("Runtime: %fs\n", time);
  return 0;
}
```

## Compile

To compile your code, simply add `-lRunTime` at the end of the command.<br>

### Example

`gcc -Wall -o runnable yourCode.c -lRunTime` .<br>
To run this code, enter `./runnable` and you are all set.
