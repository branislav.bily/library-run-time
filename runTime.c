#include <time.h>
#include "libraryRunTime.h"

clock_t timer;

void startTimer() {
  timer = clock();
}

double endTimer() {
  timer = clock() - timer;
  return ((double) timer) / CLOCKS_PER_SEC;
}
