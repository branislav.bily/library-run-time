CC=gcc
CFLAGS=-Wall -g
BINS=libRunTime.so runtime_libraryTest
all:	$(BINS)

libRunTime.so: runTime.c libraryRunTime.h
	$(CC) $(CFLAGS) -fPIC -dynamiclib -o $@ runTime.c -lc

runtime_libraryTest: libraryTest.c runTime.o
	$(CC) $(CFLAGS) -o $@ $^ -L. -lRunTime

clean: 
	rm -rf *.o $(BINS) *.dSYM
